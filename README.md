Ce tutoriel a pour but de vous guider dans l'installation de Mumble dans Tails afin de l'utiliser avec le serveur de Systemli.org.
Tout à la fin de ce tutoriel, vous trouverez les informations pour créer un channel via Mumble.

# Prérequis.
- Avoir une clé tails avec une persistance activée.
- Avoir configuré sa persistance pour que soient conservés (Dans le menu `Applications -> Outils Système -> Stockage Persistant` de Tails.)
	- Les logiciels supplémentaires
	- Les dotfiles
- Avoir démarré Tails en y ajoutant un mot de passe d'administration
- Etre connecté.e à Internet et à Tor.

# Installer Mumble.
Ouvrir un terminal `Applications -> Utilitaires -> Terminal` et saisissez :
```console
sudo apt-get update
sudo apt install mumble 
```
Selectionnez installer à chaque fois.

# Lancer Mumble.
Ouvrir Mumble depuis `Applications -> Internet -> Mumble`. 

# Faire la première étape de configuration. 
Introduction : suivant
Assistant audio : Branchez votre micro à votre prise jack et laissez vous guider.

Selection du périphérique : 
	- Periphérique d'entrée : Laisser PulseAudio et Default Input partout
	- Periphérique de sortie : Laisser PulseAudio et Default Input partout

Creation automatique d'un certificat

# Régler Mumble pour qu'il utilise le proxy de Tor.
Aller dans `Configurer -> Réglages -> Réseau`. Dans la boîte `Proxy` remplissez les champs suivants :
- Type : Socks5 
- Nom d'hôte : 127.0.0.1 
- Port : 9050
- Dans Privacy : Cocher Do not sent OS information to Mumble servers and web servers
- Dans Service de Mumble : Décocher Envoyer des statistiques anonymes
- Dans Connexion → Cocher forcer le mode TCP

Et cliquez sur `Connection`.

# Ajouter un serveur.
Aller dans `Serveur -> ajouter nouveau` et remplissez les champs suivants :
- Adresse  : talk.systemli.org
- Port : 64738
- Vous mettez le nom d'ulisateur que vous voulez, c'est comme ça qu'on vous verra.

Attention ! La liste des utilisteurs et des salons est publique sur Mumble donc ne mettez pas vos noms et prénoms comme pseudo et créez des channels avec des noms de chiens ou de chevaux de course plutôt que des trucs louches.

Cliquez sur `OK`.
     
# Se connecter à un serveur.
Dans la fenêtre `Connexion au serveur Mumble`, selectionnez le serveur que vous venez de créer et cliquer sur `Connexion`.
Là, Mumble va charger la liste des salons qui éxistent sur le serveur. Ca va s'afficher dans la colonne de droite. 
Dans la colonne de gauche vous pouvez écrire des messages. Vous y verrez aussi des messages du systême vous informant de ce qui est en train de se passer.

Attention ! Une fois connécté.e à un serveur, il ne faut pas écrire des messages dans la colonne de gauche de Mumble avant de s'être connecté à un channel, sinon vous ecrivez à tout le monde.

# Si vous devez vous connecter à un channel protégé par un mot de passe.
Allez dans `Serveur -> Jetons d'accès`. C'est là que vous entrerez le mot de passe du salon que vous allez rejoindre après. 
Cliquez sur `Ajouter`, puis taper votre mot de passe et cliquez sur `OK`.

# Se connecter à un channel. 
Dans la colonne de droite, scrollez jusqu'à trouver le channel auquel vous voulez vous connecter. Faites un clic droit dessus et cliquez sur `Rejoindre le salon`. Si le channel necessite un mot de passe, Mumble va le chercher tout seul dans sa liste et vous connecter.

# Sauvegarder la configuration de Mumble.
Dans un terminal, saisissez :
```console
mkdir -pv /live/persistence/TailsData_unlocked/dotfiles/.config/
cp -rv ~/.config/Mumble /live/persistence/TailsData_unlocked/dotfiles/.config/
```

# Desinstaller Mumble
Dans un terminal saisissez :
```console
sudo apt remove mumble
srm -rv /live/persistence/TailsData_unlocked/dotfiles/.config/Mumble
```

# Créer un channel.
Une fois connécté à un serveur, dans la colonne de droite, faites un clic droit sur le nom du serveur et cliquez sur `Ajouter`. Donnez un nom au serveur, réglez les aures paramètres si vous voulez, puis cliquez sur `OK`.

Pour protéger un channel avec un mot de passe, scrollez dans la liste des channels jusqu'à trouver celui que vous venez de créer. Faites un clic droit dessus puis cliquez sur `Modifier`. Dans l'onglet `Propriétés` vous pouvez ajouter le mot de passe que vous voulez, puis sur `OK`.
